<?php

use App\Models\Property_images;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropertyImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = ['1.JPEG', '2.JPEG', '3.JPEG', '4.JPEG', '5.JPEG'];
        $results = DB::select("SELECT * FROM property_info");
        foreach($results as $result) {
            $randInt = random_int(0, count($images)-1);
            Property_images::create([
                'house_id'=>$result->house_id,
                'user_id'=>$result->user_id,
                'img_name'=> 'property_images/'.$images[$randInt],
                'primary_image'=>'property_images/'.$images[$randInt],
                'image_caption'=>''
            ]);
        }
    }
}

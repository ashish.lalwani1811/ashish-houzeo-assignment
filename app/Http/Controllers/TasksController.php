<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class TasksController extends Controller
{
    public function getTasks() {
        try {
            $results = DB::select('SELECT title, id, updated_at, house_id from assigned_tasks');
            $i=1;
            foreach($results as $result) {
                $result->index = $i++;
            }
            return new JsonResponse(['data' => $results]);
        } catch(Exception $e) {
            return new JsonResponse("Some error occured.", 500);
        }
    }
}

// End point created
